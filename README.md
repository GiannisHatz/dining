# @universis/dining

Universis API server plugin for the dining application

## Usage

Install plugin

    npm i @universis/dining

## Configure

Register `@universis/dining#DiningService` in application services:

    # app.production.json

    "services": [
        ...,
        {
            "serviceType": "@universis/dining#DiningService"
        }
    ]

Add `@universis/dining#DiningSchemaLoader` to schema loaders

    # app.production.json
    
    {
        "settings": {
            "schema": {
                "loaders": [
                    ...,
                    {
                        "loaderType": "@universis/dining#DiningSchemaLoader"
                    }
                ]
            }
        }
    }