import { ApplicationService } from "@themost/common";
import LocalScopeAccessConfiguration from './config/scope.access.json';
export class DiningService extends ApplicationService {
    constructor(app) {
        super(app);
        // extend universis api scope access configuration
        if (app && app.container) {
            app.container.subscribe((container) => {
                if (container) {
                    // add extra scope access elements
                    const scopeAccess = app.getConfiguration().getStrategy(function ScopeAccessConfiguration() { });
                    if (scopeAccess != null) {
                        scopeAccess.elements.push.apply(scopeAccess.elements, LocalScopeAccessConfiguration);
                    }
                }
            });
        }
    }

}