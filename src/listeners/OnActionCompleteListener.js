import {DataError} from '@themost/common';
// eslint-disable-next-line no-unused-vars
async function beforeSaveAsync(event) {
    //
} 

async function afterSaveAsync(event) {
    // get context
    // eslint-disable-next-line no-unused-vars
    const context = event.model.context;
    // get current status
    const actionStatus = await event.model.where('id').equal(event.target.id).select('actionStatus/alternateName').value();
    if (actionStatus === 'CompletedActionStatus') {
        // set student dining card
        const now = new Date();        
        let academicYear = await event.model.where('id').equal(event.target.id).select('diningRequestEvent/academicYear').value();
        let academicPeriod = await event.model.where('id').equal(event.target.id).select('diningRequestEvent/academicPeriod').value();
        if (academicYear == null) {
            // get student current academic year
            academicYear = await event.model.where('id').equal(event.target.id).select('student/department/currentYear').value();
        }
        if (academicPeriod == null) {
            // get student current academic period
            academicPeriod = await event.model.where('id').equal(event.target.id).select('student/department/currentPeriod').value();
        }
        // get original student
        let student = await event.model.where('id').equal(event.target.id).select('student').value();
        const studentDiningCard = {
            action: event.target.id,           
            student: student, 
            academicYear: academicYear,
            academicPeriod: academicPeriod,
            validFrom: now,
            validThrough: new Date(now.getFullYear() + 1, now.getMonth(), now.getDay()),
            active: true
        };    
        // save student dining card    
        await context.model('StudentDiningCard').silent().save(studentDiningCard);
    }
} 

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return beforeSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}