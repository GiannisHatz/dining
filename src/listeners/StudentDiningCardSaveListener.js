/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
import {DataError} from "@themost/common";

export function beforeSave(event, callback) {
    (async () => {

        // validate student state (on insert or update)
        if (event.state === 1 || event.state === 2) {
            let context = event.model.context;
            /**
             * @type {Student|DataObject|*}
             */
            const student = context.model('Student').convert(event.target.student);
            const isActive = await student.silent().isActive();
            if (!isActive) {
                throw new DataError('ERR_STATUS',
                    'Invalid student status. Student Dining Card may be published upon an active student only.'+student
                    , null,
                    'Student', 'studentStatus');
            }
            
            const studentObject = await context.model('Student').where('id').equal(student.id).silent().flatten().getItem();
            
            const academicYear = context.model('AcademicYear').convert(event.target.academicYear).getId();
            if (Number.isInteger(academicYear) === false) {
                throw new DataError('E_VALUE', 'Invalid academic year. Expected number.', null, 'AcademicYear', 'id');
            }

            const academicYearObject =  await context.model('AcademicYear').where('id').equal(academicYear).getItem();
            if (academicYearObject === undefined){
                throw new DataError('E_VALUE', 'Invalid academic year.', null, 'AcademicYear', 'id');
            }

            if (event.target.academicPeriod != null){

                const academicPeriod = context.model('AcademicPeriod').convert(event.target.academicPeriod).getId();
                if (Number.isInteger(academicPeriod) === false) {
                    throw new DataError('E_VALUE', 'Invalid academic period. Expected number.', null, 'AcademicPeriod', 'id');
                }
    
                const academicPeriodObject =  await context.model('AcademicPeriod').where('id').equal(academicPeriod).getItem();
                if (academicPeriodObject === undefined){
                    throw new DataError('E_VALUE', 'Invalid academic period.', null, 'AcademicPeriod', 'id');
                }
                
            }


            // get academicYear year and period
            // check academicYear year and period
            //const academicYear = _.isObject(event.target.academicYear) ? event.target.academicYear.id : event.target.academicYear;
            //const academicPeriod  = _.isObject(event.target.academicPeriod) ? event.target.academicPeriod.id : event.target.academicPeriod;
            //if (academicYear<studentObject.academicYear || (academicYear===studentObject.academicYear  && academicPeriod<studentObject.academicPeriod)) {
             //   throw new DataError('ERR_STATUS',
            //        'Invalid academic year. Student academic year does not match with Student Dining Card AcademicYear.'
            //        , null,
            //        'Student', 'studentStatus');
            //}
        }
    })().then( () => {
       return callback();
    }).catch( err => {
        return callback(err);
    });
}


/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    (async () => {
        //
    })().then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}
