import {DataError} from "@themost/common";

// eslint-disable-next-line no-unused-vars
async function beforeSaveAsync(event) {

    let runAllRules = false;

    if (event.state === 2) {
        //UPDATE
        //Load current status
        const actionStatus = await event.model.where('id').equal(event.target.id).select('actionStatus/alternateName').value();
       
        //Action is not submitted yet. Run again all rules
        if (actionStatus === 'PotentialActionStatus') {
            runAllRules = true;
        }
        else if (actionStatus === 'ActiveActionStatus' || actionStatus === 'CompletedActionStatus'){
            //Dining request already submitted. Do not allow to update any property            
            removeAllInformationExceptStatus(event.target);
        }
        else if (actionStatus === 'CancelledActionStatus'){
            //Dining request already cancelled. 
            throw new DataError('E_VALUE', 'Cannot update cancelled action.', null, 'DiningRequestAction', 'actionStatus');
        }
    }
    else  if (event.state === 1) {
        runAllRules = true;
    }
  

    if (runAllRules){
        // set nullable fields to null when they don't need to have a value
        let model = event.target;
    
        if (Object.prototype.hasOwnProperty.call(model, 'militaryActive')) {
            if (model.militaryActive == true) {
                applyRulesForPrerequisites(model);
                return;
            }            
        }

        if (Object.prototype.hasOwnProperty.call(model, 'militarySchool')) {
            if (model.militarySchool == true) {
                applyRulesForPrerequisites(model);
                return;
            }            
        }
        
        if (Object.prototype.hasOwnProperty.call(model, 'studentHouseResident')) {
            if (model.studentHouseResident == true) {
                applyRulesForPrerequisites(model);
                return;
            }                        
        }
        
        if (Object.prototype.hasOwnProperty.call(model, 'sameDegreeHolder')) {
            if (model.sameDegreeHolder == true) {
                applyRulesForPrerequisites(model);
                return;
            }                        
        }

        if (Object.prototype.hasOwnProperty.call(model, 'foreignScholarStudent')) {
            if (model.foreignScholarStudent == true) {
                applyRulesForPrerequisites(model);
                return;
            }            
        }

        if (Object.prototype.hasOwnProperty.call(model, 'cyprusStudent')) {
            if (model.cyprusStudent == true) {
                applyRulesForCyprusStudentOrForeignStudentOrChildOfGreekExpatriate(model);
                return;
            }    
        }

        if (Object.prototype.hasOwnProperty.call(model, 'foreignStudentOrChildOfGreekExpatriate')) {
            if (model.foreignStudentOrChildOfGreekExpatriate == true) {           
                applyRulesForCyprusStudentOrForeignStudentOrChildOfGreekExpatriate(model);
                return;
            }        
        }

        if (Object.prototype.hasOwnProperty.call(model, 'studentReceivesUnemploymentBenefit')) {
            if (model.studentReceivesUnemploymentBenefit == true){
                model.institutionOrClubLocationSameAsStudentPermanentResidence = null;
                model.studentSubmitsTaxReportInGreece = null;
                nullifyFamilyInformation(model);
                nullifyGuardianFinancialInformation(model);
                nullifySpouseFinancialInformation(model);
                nullifyStudentFinancialInformation(model);
                return;
            }
        }

        if (Object.prototype.hasOwnProperty.call(model, 'maritalStatus')) {
            if (model.maritalStatus && model.maritalStatus.alternateName == 'single') {
                model.noOfChildren = null;
                nullifySpouseFinancialInformation(model);
                if (model.guardianType.alternateName == 'nobody') {
                    model.institutionOrClubLocationSameAsGuardianPermanentResidence = null;
                    model.guardianNoOfChildren = null;
                    model.numberOfStudyingSiblings = null;
                    nullifyGuardianFinancialInformation(model);
                }
            }
            else {
                model.guardianType = null;
                model.institutionOrClubLocationSameAsGuardianPermanentResidence = null;
                model.guardianNoOfChildren = null;
                model.numberOfStudyingSiblings = null;
                nullifyGuardianFinancialInformation(model);
                if (model.maritalStatus && model.maritalStatus.alternateName != 'married') {
                    nullifySpouseFinancialInformation(model);
                }
            }
        }
    }
    

} 

function applyRulesForPrerequisites(model) {
    //nullify all other properties except foreignScholarStudent, inscriptionModeTag, lessThan25yrs and allInformationSubmittedIsTrueConfirmation
    nullifyPersonalInformation(model);
    nullifyFamilyInformation(model);
    nullifyGuardianFinancialInformation(model);    
    nullifySpouseFinancialInformation(model);
    nullifyStudentFinancialInformation(model);    
}

function applyRulesForCyprusStudentOrForeignStudentOrChildOfGreekExpatriate(model) {
    //Only these from personal information
    // model.studentReceivesUnemploymentBenefit = null;
    // model.institutionOrClubLocationSameAsStudentPermanentResidence = null;
    // model.studentSubmitsTaxReportInGreece = null;


    // nullifyFamilyInformation(model);
    // nullifyGuardianFinancialInformation(model);
    // nullifySpouseFinancialInformation(model);

    //Only this from personal financial information
    model.studentAfm = null;
}

function nullifyPersonalInformation(model){
    model.foreignStudentOrChildOfGreekExpatriate = null;
    model.cyprusStudent = null;
    model.studentDisabledPerson = null;
    model.studentOrphan = null;
    model.childOfVictimOfTerrorism = null;
    model.studentReceivesUnemploymentBenefit = null;
    model.studentSubmitsTaxReportInGreece = null;
    model.institutionOrClubLocationSameAsStudentPermanentResidence = null;
}

function nullifyFamilyInformation(model){
    model.noOfChildren = null;
    model.maritalStatus = null;
    model.numberOfStudyingSiblings = null;
    model.guardianNoOfChildren = null;
    model.guardianType = null;
    model.institutionOrClubLocationSameAsGuadianPermanentResidence = null;
}

function nullifyGuardianFinancialInformation(model){
    model.studentGuardianTotalIncome = null;
    model.studentGuardianAfm = null;
    model.studentGuardianReceivesUnemploymentBenefit = null;
    model.studentGuardianSubmitsTaxReportInGreece = null;
}

function nullifySpouseFinancialInformation(model){
    model.studentSpouseTotalIncome = null;
    model.studentSpouseAfm = null;
    model.studentSpouseReceivesUnemploymentBenefit = null;
}

function nullifyStudentFinancialInformation(model){
    model.studentTotalIncome = null;
    model.studentAfm = null;
}

function removeAllInformationExceptStatus(model){
    delete model.militaryActive;
    delete model.militarySchool;
    delete model.studentHouseResident;
    delete model.sameDegreeHolder;
    delete model.foreignScholarStudent;
    delete model.lessThan25yrs;
    delete model.institutionOrClubLocationSameAsStudentPermanentResidence;
    delete model.studentDisabledPerson;
    delete model.studentOrphan;
    delete model.numberOfStudyingSiblings ;
    delete model.maritalStatus;
    delete model.noOfChildren;
    delete model.guardianType;
    delete model.institutionOrClubLocationSameAsGuadianPermanentResidence;
    delete model.guardianNoOfChildren;
    delete model.studentReceivesUnemploymentBenefit;
    delete model.childOfVictimOfTerrorism;
    delete model.studentSubmitsTaxReportInGreece;
    delete model.studentGuardianSubmitsTaxReportInGreece;
    delete model.studentAfm;
    delete model.studentTotalIncome;
    delete model.studentSpouseReceivesUnemploymentBenefit;
    delete model.studentSpouseAfm;
    delete model.studentSpouseTotalIncome;
    delete model.studentGuardianReceivesUnemploymentBenefit ;
    delete model.cyprusStudent;
    delete model.foreignStudentOrChildOfGreekExpatriate;
    delete model.studentGuardianAfm;
    delete model.studentGuardianTotalIncome;
    delete model.allInformationSubmittedIsTrueConfirmation;
}

async function afterSaveAsync(event) {
    //
} 

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return beforeSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}